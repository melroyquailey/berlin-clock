package com.inkglobal.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BerlinClockImplTest {
	
	/**
	 * Input       Expected Result
	 * 00:00:00    Y
	 *             OOOO
	 *             OOOO
	 *             OOOOOOOOOOO
	 *             OOOO
	 */
	@Test
	public void testCase1() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("00:00:00");
		
		assertEquals("Y", unitToTest.getSecondsIndicatorRow());
		assertEquals("OOOO", unitToTest.getHoursRow1());
		assertEquals("OOOO", unitToTest.getHoursRow2());
		assertEquals("OOOOOOOOOOO", unitToTest.getMinutesRow1());
		assertEquals("OOOO", unitToTest.getMinutesRow2());
		
	}
	
	/**
	 * Input       Expected Result
	 * 13:17:01    O
	 *             RROO
	 *             RRRO
	 *             YYROOOOOOOO
	 *             YYOO
	 */
	@Test
	public void testCase2() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("13:17:01");
		
		assertEquals("O", unitToTest.getSecondsIndicatorRow());
		assertEquals("RROO", unitToTest.getHoursRow1());
		assertEquals("RRRO", unitToTest.getHoursRow2());
		assertEquals("YYROOOOOOOO", unitToTest.getMinutesRow1());
		assertEquals("YYOO", unitToTest.getMinutesRow2());
		
	}

	/**
	 * Input       Expected Result
	 * 23:59:59    O
	 *             RRRR
	 *             RRRO
	 *             YYRYYRYYRYY
	 *             YYYY
	 */
	@Test
	public void testCase3() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("23:59:59");
		
		assertEquals("O", unitToTest.getSecondsIndicatorRow());
		assertEquals("RRRR", unitToTest.getHoursRow1());
		assertEquals("RRRO", unitToTest.getHoursRow2());
		assertEquals("YYRYYRYYRYY", unitToTest.getMinutesRow1());
		assertEquals("YYYY", unitToTest.getMinutesRow2());
		
	}

	/**
	 * Input       Expected Result
	 * 24:00:00    O
	 *             RRRR
	 *             RRRR
	 *             OOOOOOOOOOO
	 *             OOOO
	 */
	@Test
	public void testCase4() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("24:00:00");
		
		assertEquals("Y", unitToTest.getSecondsIndicatorRow());
		assertEquals("RRRR", unitToTest.getHoursRow1());
		assertEquals("RRRR", unitToTest.getHoursRow2());
		assertEquals("OOOOOOOOOOO", unitToTest.getMinutesRow1());
		assertEquals("OOOO", unitToTest.getMinutesRow2());
	}
	
	/**
	 * Input       Expected Result
	 * -1:00:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidHours1() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("-1:00:00");
	}
	
	/**
	 * Input       Expected Result
	 * X0:00:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidHours2() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("X0:00:00");
	}
	
	/**
	 * Input       Expected Result
	 * X0:00:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidHours3() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("0X:00:00");
	}
	
	/**
	 * Input       Expected Result
	 * XX:00:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidHours4() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("XX:00:00");
	}
	
	/**
	 * Input       Expected Result
	 * XX:00:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidHours5() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("25:00:00");
	}
	
	/**
	 * Input       Expected Result
	 * 14:-1:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidMinutes1() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:-1:00");
	}
	
	/**
	 * Input       Expected Result
	 * 14:X0:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidMinutes2() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:X0:00");
	}
	
	/**
	 * Input       Expected Result
	 * 14:0X:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidMinutes3() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:0X:00");
	}
	
	/**
	 * Input       Expected Result
	 * 14:XX:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidMinutes4() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:XX:00");
	}
	
	/**
	 * Input       Expected Result
	 * 14:60:00    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidMinutes5() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:60:00");
	}
	
	/**
	 * Input       Expected Result
	 * 14:00:-1    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidSeconds1() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:00:-1");
	}
	
	/**
	 * Input       Expected Result
	 * 14:00:X0    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidSeconds2() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:00:X0");
	}
	
	/**
	 * Input       Expected Result
	 * 14:00:0X    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidSeconds3() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:00:0X");
	}
	
	/**
	 * Input       Expected Result
	 * 14:00:XX    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidSeconds4() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:00:XX");
	}
	
	/**
	 * Input       Expected Result
	 * 14:00:60    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidSeconds5() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("14:00:60");
	}
	
	/**
	 * Input       Expected Result
	 * 1:00:60    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidFormat1() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("1:00:60");
	}
	
	/**
	 * Input       Expected Result
	 * 011:00:60    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidFormat2() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("011:00:60");
	}
	
	/**
	 * Input       Expected Result
	 * 01:1:60    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidFormat3() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("01:1:60");
	}
	
	/**
	 * Input       Expected Result
	 * 01:111:60    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidFormat4() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("01:111:60");
	}
	
	/**
	 * Input       Expected Result
	 * 01:11:1    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidFormat5() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("01:11:1");
	}
	
	/**
	 * Input       Expected Result
	 * 01:11:111    IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidFormat6() {
		BerlinClockImpl unitToTest = new BerlinClockImpl("01:11:111");
	}
	
	
}

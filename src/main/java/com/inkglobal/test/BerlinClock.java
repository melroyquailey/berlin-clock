package com.inkglobal.test;

/**
 * Sample application to create a representation of the Berlin Clock for a given time (hh::mm:ss).   
 * 
 * @author Melroy Quailey
 *
 */
public class BerlinClock {
	public static void main(String[] args) {
		
		if (args.length == 0) {
			System.out.println("Usage: com.inkglobal.test.BerlinClock hh:mm:ss");
		} else {
			
			Clock berlinClock = ClockBuilderFactory.build(ClockBuilderFactory.ClockType.BERLIN_CLOCK, args[0]);
			System.out.println(berlinClock);

		}
	}
}

package com.inkglobal.test;

import java.text.ParseException;

/**
 * Factory to build concrete implementations of the Clock interface
 * 
 * @author Melroy Quailey
 *
 */
public class ClockBuilderFactory {
	
	// enum to define different clock types for future implementation
	public enum ClockType {
		BERLIN_CLOCK
	}

	/**
	 * Returns a concrete implementation of the Clock interface for the given ClockType parameter initializing 
	 * the with the given time string 
	 * @param args 
	 * 
	 * @param ClockType - The type of clock to return
	 * @param String - representing a time to initialize the clock to
	 * @return Implementation of the Clock interface
	 * @throws ParseException - throws parse exception if the time parameter is in a format unrecognized 
	 */
	public static Clock build(ClockType clockType, String time) {
		
		Clock clockToReturn = null;
		
		switch (clockType) {
		case BERLIN_CLOCK:
			clockToReturn = new BerlinClockImpl(time);
			break;
		}
		
		return clockToReturn;
	}
	
	
}

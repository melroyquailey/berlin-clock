package com.inkglobal.test;

import java.text.ParseException;

/**
 * 
 * Concrete implementation of Clock interface
 * 
 * Instances of this class represent the time as a Berlin Clock
 * 
 * @author Melroy Quailey
 *
 */
public class BerlinClockImpl implements Clock {
	
	private static String TIME_REGEX = "(0[0-9]:[0-5][0-9]:[0-5][0-9]|1[0-9]:[0-5][0-9]:[0-5][0-9]|2[0-3]:[0-5][0-9]:[0-5][0-9]|24:00:00)";

	private String secondsIndicatorRow;
	private String hoursRow1;
	private String hoursRow2;
	private String minutesRow1;
	private String minutesRow2;

	/**
	 * Instantiates a new object of type BerlinClockImpl initializing it to 
	 * the time parameter specified.
	 * 
	 * @param time
	 * @throws IllegalArgumentException
	 */
	public BerlinClockImpl(String time) throws IllegalArgumentException {
		try {
			parse(time);
		} catch (ParseException pe) {
			throw new IllegalArgumentException("Unable to initialize BerlinClockImpl: time for " + "'" + time + "' is invalid. "
					+ "Format should be of the form hh:mm:ss, i.e 21:34:59."
					, pe);
		}
	}
	
	/*
	 * Parse the time parameter to initialize is instance of BerlinClockImpl
	 */
	private void parse(String time) throws ParseException {
		// if the time parameter is invalid throw exception
		if (null != time && time.matches(TIME_REGEX)) {
			processSecondsIndicator(time);
			processHours(time);
			processMinutes(time);
		} else {
			throw new ParseException(time, 0);
		}
	}
	
	/*
	 * Initializes the seconds indicator for this instance of BerlinClockImpl
	 */
	private void processSecondsIndicator(String time) {
		if (Integer.parseInt(time.substring(6)) % 2 == 0) {
			secondsIndicatorRow = "Y";
		} else {
			secondsIndicatorRow = "O";
		}
	}
	
	/*
	 * Initializes the hours rows for this instance of BerlinClockImpl
	 */
	private void processHours(String time) {
		int hours = Integer.parseInt(time.substring(0,2));
		int fiveMinIntervals = hours / 5;
		
		char[] charArray1 = new char[] {'O','O','O','O'};
		
		for (int i = 0; i < fiveMinIntervals; i++) {
			charArray1[i] = 'R';
		}
		
		hoursRow1 = new String(charArray1);
		
		int remainder = hours % 5;
		
		char[] charArray2 = new char[] {'O','O','O','O'};
		for (int i = 0; i < remainder; i++) {
			charArray2[i] = 'R';
		}
		
		hoursRow2 = new String(charArray2);
	}
	
	/*
	 * Initializes the minutes rows for this instance of BerlinClockImpl
	 */
	private void processMinutes(String time) {
		int minutes = Integer.parseInt(time.substring(3,5));
		
		char[] charArray1 = new char[] {'O','O','O','O','O','O','O','O','O','O','O'};
		int fiveMinIntervals = minutes / 5;
		
		for (int i = 0; i < fiveMinIntervals; i++) {
			if (i == 2 || i == 5 || i == 8) {
				charArray1[i] = 'R';
			} else {
				charArray1[i] = 'Y';
			}
		}
		
		minutesRow1 = new String(charArray1);

		int minutesRemainder = minutes % 5;
		char[] charArray2 = new char[] {'O','O','O','O'};
		
		for (int i = 0; i < minutesRemainder; i++) {
			charArray2[i] = 'Y';
		}

		minutesRow2 = new String(charArray2);
		
	}

	public String getSecondsIndicatorRow() {
		return secondsIndicatorRow;
	}

	public String getHoursRow1() {
		return hoursRow1;
	}

	public String getHoursRow2() {
		return hoursRow2;
	}

	public String getMinutesRow1() {
		return minutesRow1;
	}

	public String getMinutesRow2() {
		return minutesRow2;
	}

	@Override
	public String toString() {
		return secondsIndicatorRow + "\n" + hoursRow1 + "\n"
				+ hoursRow2 + "\n" + minutesRow1 + "\n" + minutesRow2 + "\n";
	}
}
